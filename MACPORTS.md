(c) Stefan Countryman, 2019

# Instructions on editing Portfiles for MacPorts

- When updating version numbers for this module:
  - Make sure that a tag exists in the git repository for the new version.
  - Regenerate the rmd160 and sha256 hashes for the distribution file (the
    tarball) using:
    - `openssl dgst -rmd160 $DISTFILENAME`
    - `openssl dgst -sha256 $DISTFILENAME`
    - `ls -nl $DISTFILENAME | awk '{print $5}'`
- Make sure that all dependencies are compatible with the python versions you
  specify in python.versions.
- Update the default python version to the newest stable version when possible
- Note that the preferred homepage and master_site might change if the
  maintainer changes/project switches hands

# Testing a Portfile locally

Instructions taken from
[the MacPorts documentation](https://guide.macports.org/chunked/development.local-repositories.html).

1. Since this is a python port, MacPorts expects to find the portfile in
   `${ARBITRARY_PREFIX}/python/${name}/Portfile` where `${name}` is the name of the
   port. So, to test, copy all your local Portfiles to directories following
   this pattern. For example, with username `Stefan`, local port repository in
   `~/dev/local-ports/`, and port name `py-gpstime`, you would copy the
   portfile to `/Users/Stefan/dev/local-ports/python/py-gpstime/Portfile`.
2. Insert the path to the local ports repository into
   `/opt/local/etc/macports/sources.conf` by adding the line
   `file:///Users/Stefan/dev/local-ports` (preferably ahead of the other sources
   listed).
3. Use portindex in the local repository's directory to create or update the
   index of the ports in your local repository:
   ```bash
   cd ~/dev/local-ports
   portindex
   ```
4. Try searching for and installing the new port as you ordinarily would, e.g.
   `port search py-gpstime`.
